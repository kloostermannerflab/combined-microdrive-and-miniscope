# README #


### What is this repository for? ###

* This repository contains STL files for 3D-print components of a microdrive array designed for combined electrophysiological signal recording and miniscope calcium imaging. It also contains custom python codes for data analysis acquired by combined recording experiments.
* Anyone who wants to perform the experiments and analyze data can freely download the files.


### Who do I talk to? ###

* If you have any questions or want to give us feedback, please contact chaeyoung.kim@brain.snu.ac.kr